Connector =
{
    FormData:
    {
        Name: { Type: "text", Title: "Name", Value: "Travis CI" },
        ServerUrl: { Type: "text", Title: "API URL", Value: "https://api.travis-ci.org/", Required: true, Restart: true, ProtocolCheck: true },
        Username: { Type: "text", Title: "Username", Required: true, Restart: true },
        Token: { Type: "password", Title: "API Token", Required: true, Restart: true },
        ImportantRefs: { Type: "regex", Title: "Important Branches (Regex)", Value: "(master|develop|release/(.*))" },
        OnlyActive: { Type: "checkbox", Title: "Show only active projects", Value: false, Required: true, Restart: true },
        Interval: { Type: "number", Title: "Request Interval (in milliseconds)", Value: 1000, Required: true }
    },

    Index: 0,

    Shared:
    {
        StateMap:
        {
            "created": "Pending",
            "passed": "Success",
            "started": "Running",
            "failed": "Failed"
        }
    },

    _refreshTimer: null,
    _refreshing: false,
    _serverUrl: null,
    _travisUrl: null,

    _responseProcessors: null,
    _projects: null,
    _users: null,
    _pendingRequests: null,

    Logger: null,
    Requestor: null,
    DataSink: null,
    ConnectorConfig: null,

    Initialize: function()
    {
        this._pendingRequests = [];
        this._projects = {};
        this._users = {};
        this._organizations = {};
        this._responseProcessors = {};
        this._responseProcessors.Repositories = this.ParseRepositories;
        this._responseProcessors.Builds = this.ParseBuilds;
        this._responseProcessors.Branches = this.ParseBranches;
        this._responseProcessors.User = this.ParseUser;
    },

    Start: function()
    {
        if (this._refreshTimer != null)
        {
            this.Logger.Log("Already started.", this.Logger.LogLevels.Warning, this);
            return;
        }

        this._serverUrl = this.ConnectorConfig.ServerUrl + (this.ConnectorConfig.ServerUrl && this.ConnectorConfig.ServerUrl.endsWith("/") ? "" : "/");
        this._travisUrl = this._serverUrl.replace("api.", "").replace("/api/", "/");

        this._refreshTimer = setInterval(this.Refresh.bind(this), this.ConnectorConfig.Interval);
        this.Refresh();
    },

    Stop: function()
    {
        if (this._refreshTimer == null)
        {
            return;
        }

        clearInterval(this._refreshTimer);
        this._refreshTimer = null;
    },

    Refresh: function()
    {
        if (this._refreshing)
        {
            this.Logger.Log("Already refreshing, skipping...", this.Logger.LogLevels.Info, this);
            return;
        }

        var pendingRequest = this._pendingRequests.pop();
        if (pendingRequest)
        {
            this.RequestInformation(pendingRequest.Processor, pendingRequest.Identifier, this._serverUrl + pendingRequest.Uri);
        }
        else
        {
            this.RequestInformation("Repositories", 0, this._serverUrl + "repos?include=owner.avatar_url");
        }
    },

    RequestFailed: function(identifier)
    {
        this._refreshing = false;
        this.Logger.Log("Request for " + identifier + " failed.", this.Logger.LogLevels.Warning, this);
    },

    RequestInformation: function(requestType, identifier, url, callbackDone)
    {
        this._refreshing = true;

        this.Logger.Log("Requesting " + requestType + " (" + identifier + ")...", this.Logger.LogLevels.Debug, this);

        var options = {
            Headers: {
                "Travis-API-Version": 3,
                "Authorization": "token " + this.ConnectorConfig.Token
            },
            DataType: "json",
            MimeType: "application/json"
        };

        var self = this;
        this.Requestor.Get(url,
            function(data)
            {
                self._refreshing = false;
                App.ShowWarning(false);

                var processor = self._responseProcessors[requestType];
                if (processor)
                {
                    processor.call(self, identifier, data);
                }
                else
                {
                    self.Logger.Log("No response processor for " + requestType + " found.", self.Logger.LogLevels.Error, self);
                }

                if (callbackDone)
                {
                    callbackDone(data);
                }
            },
            function(jqXhr, textStatus, errorThrown)
            {
                self.ParseErrorResponse.call(self, identifier, jqXhr.responseJSON, textStatus, errorThrown);
            },
            function(data, textStatus)
            {
                self._refreshing = false;
                if (requestType === "build" || requestType === "status" && self._projects[identifier].Status === "NoBuilds")
                {
                    if (!self._updatedProjects[identifier]) { self._updatedProjects[identifier] = { }; }
                    self._updatedProjects[identifier].Status = textStatus === "Success" ? "Updated" : "Failed";
                }
            },
            options
        );
    },

    ParseErrorResponse: function(identifier, data, textStatus, errorThrown)
    {
        this.RequestFailed(identifier);

        var message;
        if (data && data["@type"] === "error" && data.error_message)
        {
            message = "[" + this.ConnectorConfig.Name + "] " + data.error_message;
            App.ShowWarning(true, message);
            this.Logger.Log(message, this.Logger.LogLevels.Warning, this);
        }
        else
        {
            message = errorThrown === "Forbidden"
                ? "Forbidden, check Travis API token"
                : errorThrown + " (press F12 to check console)";
            App.ShowWarning(true, "[" + this.ConnectorConfig.Name + "] " + textStatus.charAt(0).toUpperCase() + textStatus.slice(1) + ": " + message);
            this.Logger.Log(message, this.Logger.LogLevels.Warning, this);
        }
    },

    CleanUri: function(uri)
    {
        return uri && uri.length && uri[0] === "/" ? uri.substr(1) : uri;
    },

    // eslint-disable-next-line complexity
    ParseRepositories: function(identifier, data)
    {
        this.Logger.Log("Parsing project list...", this.Logger.LogLevels.Debug, this);

        if (data["@type"] !== "repositories" || !data.repositories)
        {
            this.Logger.Log("Expected 'repositories', got '" + data["@type"] + "'.", this.Logger.LogLevels.Warning, this);
            return;
        }

        var projects = {};
        for (var index in data.repositories)
        {
            var element = data.repositories[index];
            if (this.ConnectorConfig.OnlyActive && !element.active)
            {
                continue;
            }

            var project = this._projects[element.id] ? this._projects[element.id] : { };
            projects[element.id] = project;
            project.Identifier = element.id;
            project.Name = element.name;
            project.Url = this._travisUrl + element.slug;
            project.Username = project.Username || element.slug.split("/")[0];
            project.Reference = project.Reference || (element.default_branch && element.default_branch.name ? element.default_branch.name : "");
            project.Image = element.owner && element.owner.avatar_url ? element.owner.avatar_url : "";

            var uri = this.CleanUri(element["@href"]);
            this._pendingRequests.unshift({ Processor: "Builds", Identifier: project.Identifier, Uri: uri + "/builds?include=created_by.avatar_url&limit=2" });
            this._pendingRequests.unshift({ Processor: "Branches", Identifier: project.Identifier, Uri: uri + "/branches" });
        }

        this._projects = projects;
        this.DataSink.Update(this._projects, null, this);
    },

    // eslint-disable-next-line complexity
    ParseBuilds: function(projectIdentifier, data)
    {
        this.Logger.Log("Parsing builds for project " + projectIdentifier + "...", this.Logger.LogLevels.Debug, this);

        var project = this._projects[projectIdentifier];

        if (!data.builds || !data.builds.length)
        {
            project.Status = "Created";
            this.DataSink.Update(this._projects, project.Identifier, this);
            return;
        }

        var latest = data.builds[0];
        project.Reference = latest.number ? "#" + latest.number + " " : "";
        project.Reference = project.Reference + (latest.branch && latest.branch.name ? latest.branch.name : "");
        project.StartedAt = latest.started_at || latest.updated_at;
        project.FinishedAt = latest.finished_at;
        project.Status = this.Shared.StateMap[latest.state] || "Unknown";

        if (latest.created_by)
        {
            project.Username = latest.created_by.name ? latest.created_by.name : latest.created_by.login;
            project.Avatar = latest.created_by.avatar_url ? latest.created_by.avatar_url : "";
        }

        this.DataSink.Update(this._projects, project.Identifier, this);
    },

    ParseBranches: function(projectIdentifier, data)
    {
        if (data["@type"] !== "branches" || !data.branches)
        {
            this.Logger.Log("Expected 'branches', got '" + data["@type"] + "'.", this.Logger.LogLevels.Warning, this);
            return;
        }

        var project = this._projects[projectIdentifier];

        var importantRefsFailed = false;
        for (var index in data.branches)
        {
            var branch = data.branches[index];
            if (branch.name.match(this.ConnectorConfig.ImportantRefs))
            {
                importantRefsFailed = importantRefsFailed || branch.last_build && branch.last_build.state === "failed";
            }
        }

        project.ImportantRefStatus = importantRefsFailed ? "Failed" : "Success";
        this.DataSink.Update(this._projects, project.Identifier, this);
    }
};